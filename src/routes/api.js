const express = require('express');
const multer = require('multer');
const crypto = require('crypto');
const fs = require('fs');
const bodyParser = require('body-parser');

const router = express.Router();
const upload = multer({ dest: '/tmp'})

/**
 * PING end point to test the API live status
 */
router.get('/ping', (req, res) => {
  res.status(200).send({
    success: true,
    timestamp: Date.now(),
    data: 'pong'
  });
});


/**
 * Post JSON
 */
router.post('/postJSON', bodyParser.json({limit: '1mb'}), (req, res) => {
  console.log(req.body)
  res.status(200).send({
    success: true,
    timestamp: Date.now()
  });
});


/**
 * Upload file
 */
router.post('/upload', upload.single('myfile'), async (req, res) => {
  try {
    console.log(req)
    let checksum = await calculateChecksum(req.file.path)
    console.log(checksum)
    res.status(200).send({ "file": req.file, "checksum": checksum});
  }catch(err) {
    res.status(400).send();
  }
});

const calculateChecksum = ((filePath) => {
  return new Promise((resolve, reject) => {
    try {
      let hash = crypto.createHash('md5')
      let stream = fs.createReadStream(filePath)
      stream.on('data', (data) => {
        hash.update(data, 'utf8')
      })
      stream.on('end', () => {
        resolve(hash.digest('base64'))
      })
    } catch (error) {
      console.error(error)
      reject(error)
    }
  })
})

module.exports = router;
