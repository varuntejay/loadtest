const express = require('express');
const routes = require('./src/routes')
const app = express();
const port = process.env.SERVER_PORT || 9086;

boot = () => {
    intialize();
    registerRoutes();
    server();
}

/**
 * use body parser json to accept application/json body
 */
intialize = () => {    
}

/**
 * reguste the API rouutes
 */
registerRoutes = () => {
    app.use('/', routes)
}

/**
 * start the express server
 */
server = () => {
    app.listen(port, () => {
        console.log('Listening on Port: ' + port);        
    })
}

/**
 * Boot the API application
 */
boot();
